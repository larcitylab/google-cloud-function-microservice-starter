// const webpack = require("webpack");
const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const TsConfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const DirectoryNamedWebpackPlugin = require('directory-named-webpack-plugin');
const _ = require("lodash");

console.log(
  "Library publish path (resolved)::",
  path.resolve(__dirname, "lib")
);

// Paths to ignore
const patternsToIgnore = [
  new RegExp(path.resolve(__dirname, 'node_modules/firebase-functions')),
  new RegExp(path.resolve(__dirname, 'node_modules')),
];

console.log("Node modules path::", path.resolve(
  __dirname,
  "node_modules"
));

console.log("patterns to ignore::", patternsToIgnore);

var defaults = {
  entry: path.resolve(__dirname, "src/index.ts"),
  stats: "verbose",
  devtool: 'inline-source-map',
  plugins: [
    new CleanWebpackPlugin([path.resolve(__dirname, "lib")], {
      root: path.resolve(__dirname, "."),
      verbose: true,
    }),
    // new webpack.IgnorePlugin(...patternsToIgnore)
  ],
  node: {
    console: true,
    global: true,
    path: true,
    // fs: true,
    url: true,
    process: true,
    Buffer: true,
    setImmediate: true,
    // __filename: true,
    // __dirname: true,
  },
  module: {
    rules: [{
      test: /\.node$/,
      loader: 'awesome-node-loader'
    }, {
      test: /\.ts$/,
      use: "ts-loader",
      // include: [path.resolve(__dirname, "src")],
      // exclude: [
      //   path.resolve(__dirname, "lib"),
      //   path.resolve(__dirname, "node_modules"),
      //   path.resolve(__dirname, "node_modules"),
      //   /.*.spec.ts$/
      // ],
      exclude: /node_modules|.*.spec.ts$/
    }, ],
  },
  resolve: {
    symlinks: true,
    modules: [
      path.resolve(
        __dirname,
        "node_modules"
      ) /*path.resolve(__dirname, 'node_modules')*/ ,
    ],
    extensions: [".ts"],
    // descriptionFiles: ["package.json"],
    plugins: [
      new TsConfigPathsPlugin({
        configFile: path.resolve(__dirname, 'tsconfig.json')
      }),
      new DirectoryNamedWebpackPlugin(),
    ]
  },
  output: {
    path: path.resolve(__dirname, "lib"),
    filename: '[name].js'
  },
  // target: "node",
};

module.exports.defaults = defaults;

module.exports.extend = function merge(config) {
  return _.extend({}, defaults, config);
};

module.exports.merge = function merge(config) {
  return _.merge({}, defaults, config);
};