import { RequestHandler } from 'express';
import { TestMiddleStack } from './middleware/test';

export const TestEndpoint: RequestHandler[] = [
  ...TestMiddleStack,
  (req, res) => {
    return res.json({
      success: true,
      time: new Date().getTime(),
      url: req.originalUrl,
    });
  },
];
