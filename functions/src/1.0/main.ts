import * as CORS from 'cors';
import { CorsOptions } from 'cors';
import * as express from 'express';
import { TestEndpoint } from './test';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

/**
 * Configure CORS
 */
const corsOptions: CorsOptions = {
  optionsSuccessStatus: 200,
  origin: (origin, callback) => {
    console.warn('@TODO check origin against whitelist::', origin);
    callback(null, true);
  },
};
app.use(CORS(corsOptions));

/**
 * /api/1.0/
 */
app.get('/', TestEndpoint);

export const Main = app;
