import { RequestHandler } from "express";

export const TestMiddleStack: RequestHandler[] = [
  (req, res, next) => {
    console.warn("endpoint::", req.originalUrl);
    next();
  },
];
