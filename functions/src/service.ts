import { Main } from './1.0';
import * as express from 'express';
import CacheWatcher from './utils/cache-watcher';

const service = express();

/**
 * v1.0
 */
service.use('/api/1.0', Main);

if (/^(dev|debug)/i.test(process.env.NODE_ENV)) {
  CacheWatcher.run();
}

const PORT = process.env.PORT || '8080';

service.listen(PORT, () => console.log(`Microservice listening @${PORT}`));
