import * as functions from "firebase-functions";
import * as express from "express";
import { Main } from "./1.0";

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const microservice = express();

// v1.0
microservice.use("1.0", Main);

// Publish
export const mailchimp = functions.https.onRequest(microservice);
