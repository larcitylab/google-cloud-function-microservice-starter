import * as chokidar from "chokidar";
import * as path from "path";

const srcPath = path.join(process.cwd(), "src");

export const run = () => {
  const watcher = chokidar.watch(`${srcPath}/**/**/*.ts`);

  watcher.on("ready", () => {
    watcher.on("all", () => {
      Object.keys(require.cache).forEach(id => {
        if (/(functions[\/\\])?src[\/\\].*\.ts$/.test(id)) {
          console.warn(`Deleting cache:: ${id}`);
          delete require.cache[id];
        }
      });
    });
  });
};

export const CacheWatcher = { run };

export default CacheWatcher;
