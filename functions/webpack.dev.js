const base = require('./webpack.base');

console.log('Configurations::', base.merge({
  mode: 'test-config'
}));

module.exports = base.merge({
  mode: 'development',
  optimization: {
    minimize: false
  }
});