// const webpack = require("webpack");
const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const TsConfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const DirectoryNamedWebpackPlugin = require('directory-named-webpack-plugin');
const _ = require("lodash");

console.log(
  "Library publish path (resolved)::",
  path.resolve(__dirname, "./functions/lib")
);

// Paths to ignore
const patternsToIgnore = [
  new RegExp(path.resolve(__dirname, './functions/node_modules/firebase-functions')),
  new RegExp(path.resolve(__dirname, './functions/node_modules')),
];

console.log("patterns to ignore::", patternsToIgnore);

var defaults = {
  entry: path.resolve(__dirname, "./functions/src/index.ts"),
  stats: "verbose",
  // devtool: 'inline-source-map',
  // context: path.resolve(__dirname, './functions'),
  context: path.resolve(__dirname),
  node: {
    path: true,
    fs: true,
    // url: true,
    process: true,
    Buffer: true,
    setImmediate: true,
    __filename: true,
    __dirname: true,
  },
  plugins: [
    new CleanWebpackPlugin([path.resolve(__dirname, "./functions/lib")], {
      root: path.resolve(__dirname, "./functions"),
      verbose: true,
    }),
    // new webpack.IgnorePlugin(...patternsToIgnore)
  ],
  module: {
    rules: [{
      test: '/\.node$/',
      loader: 'awesome-node-loader'
    }, {
      test: "/.tsx?$/",
      use: "ts-loader",
      include: [path.resolve(__dirname, "./functions/src")],
      exclude: [
        path.resolve(__dirname, "./functions/lib"),
        path.resolve(__dirname, "./functions/node_modules"),
        path.resolve(__dirname, "./functions/node_modules"),
        /.*.spec.ts$/
      ],
    }, ],
  },
  resolve: {
    symlinks: true,
    modules: [
      path.resolve(
        __dirname,
        "./functions/node_modules"
      ) /*path.resolve(__dirname, '../node_modules')*/ ,
    ],
    extensions: [".ts"],
    // descriptionFiles: ["package.json"],
    plugins: [
      new TsConfigPathsPlugin({
        configFile: path.resolve(__dirname, './functions/tsconfig.json')
      }),
      new DirectoryNamedWebpackPlugin(),
    ]
  },
  output: {
    path: path.resolve(__dirname, "./functions/lib"),
    filename: '[name].js'
  },
  target: "node",
};

module.exports.defaults = defaults;

module.exports.extend = function merge(config) {
  return _.extend({}, defaults, config);
};

module.exports.merge = function merge(config) {
  return _.merge({}, defaults, config);
};