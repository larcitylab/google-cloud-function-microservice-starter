const path = require('path');
const base = require('./webpack.base');

module.exports = base.merge({
  mode: 'development',
  optimization: {
    minimize: false
  }
});