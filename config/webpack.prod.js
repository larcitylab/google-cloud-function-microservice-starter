const path = require("path");
const base = require("./webpack.base");

module.exports = base.merge({
  mode: "production",
  stats: "minimal"
});