# Mailchimp Functions

## TypeScript Version

`~2.8.3`

## Backlog

- Working on defining webpack context, so config files can be put away in the `config` directory at the app root. To that end, please keep the `./config/webpack*` files while this work progresses.
- Webpack build still failing, although tsc build against `tsconfig.json` works OK. Will proceed with that for now.
